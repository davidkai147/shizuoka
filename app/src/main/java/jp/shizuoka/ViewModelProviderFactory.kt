package jp.shizuoka

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import jp.shizuoka.data.DataManager
import jp.shizuoka.ui.about.AboutViewModel
import jp.shizuoka.ui.feed.FeedViewModel
import jp.shizuoka.ui.feed.blogs.BlogViewModel
import jp.shizuoka.ui.feed.opensource.OpenSourceViewModel
import jp.shizuoka.ui.login.LoginViewModel
import jp.shizuoka.ui.main.MainViewModel
import jp.shizuoka.ui.main.rating.RateUsViewModel
import jp.shizuoka.ui.splash.SplashViewModel
import jp.shizuoka.utils.rx.SchedulerProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelProviderFactory @Inject
constructor(
    private val dataManager: DataManager,
    private val schedulerProvider: SchedulerProvider
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AboutViewModel::class.java)) {
            return AboutViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(FeedViewModel::class.java)) {

            return FeedViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(BlogViewModel::class.java)) {

            return BlogViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(RateUsViewModel::class.java)) {

            return RateUsViewModel(dataManager, schedulerProvider) as T
        } else if (modelClass.isAssignableFrom(OpenSourceViewModel::class.java)) {

            return OpenSourceViewModel(dataManager, schedulerProvider) as T
        } else
            if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {

                return SplashViewModel(dataManager, schedulerProvider) as T
            }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }
}