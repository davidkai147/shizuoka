package jp.shizuoka.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import jp.shizuoka.data.local.db.dao.OptionDao
import jp.shizuoka.data.local.db.dao.QuestionDao
import jp.shizuoka.data.local.db.dao.UserDao
import jp.shizuoka.data.model.db.Option
import jp.shizuoka.data.model.db.Question
import jp.shizuoka.data.model.db.User

@Database(entities = [User::class, Question::class, Option::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun optionDao(): OptionDao

    abstract fun questionDao(): QuestionDao

    abstract fun userDao(): UserDao
}