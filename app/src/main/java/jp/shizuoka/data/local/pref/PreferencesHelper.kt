package jp.shizuoka.data.local.pref

import jp.shizuoka.data.DataManager

interface PreferencesHelper {

    var accessToken: String?

    var currentUserEmail: String?

    var currentUserId: Long?

    val currentUserLoggedInMode: Int

    var currentUserName: String?

    var currentUserProfilePicUrl: String?

    var isDoneFirstLoad: Boolean

    fun setCurrentUserLoggedInMode(mode: DataManager.LoggedInMode)

    fun setFirstLoadDone(isDone: Boolean)
}