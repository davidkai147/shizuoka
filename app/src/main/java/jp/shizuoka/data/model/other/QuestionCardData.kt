package jp.shizuoka.data.model.other

import jp.shizuoka.data.model.db.Option
import jp.shizuoka.data.model.db.Question

class QuestionCardData(var question: Question, var options: List<Option>) {

    var mShowCorrectOptions: Boolean = false
}