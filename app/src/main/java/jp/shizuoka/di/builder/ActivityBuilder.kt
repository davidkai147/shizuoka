package jp.shizuoka.di.builder

import jp.shizuoka.ui.about.AboutFragmentProvider
import jp.shizuoka.ui.feed.FeedActivity
import jp.shizuoka.ui.feed.FeedActivityModule
import jp.shizuoka.ui.feed.blogs.BlogFragmentProvider
import jp.shizuoka.ui.feed.opensource.OpenSourceFragmentProvider
import jp.shizuoka.ui.login.LoginActivity
import jp.shizuoka.ui.main.MainActivity
import jp.shizuoka.ui.main.rating.RateUseDialogProvider
import jp.shizuoka.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(
        modules = [FeedActivityModule::class,
            BlogFragmentProvider::class, OpenSourceFragmentProvider::class]
    )
    internal abstract fun bindFeedActivity(): FeedActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [AboutFragmentProvider::class, RateUseDialogProvider::class])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindSplashActivity(): SplashActivity
}