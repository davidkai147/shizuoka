package jp.shizuoka.ui.about

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import jp.shizuoka.BR
import jp.shizuoka.R
import jp.shizuoka.ViewModelProviderFactory
import jp.shizuoka.databinding.FragmentAboutBinding
import jp.shizuoka.ui.base.BaseFragment
import javax.inject.Inject

class AboutFragment : BaseFragment<FragmentAboutBinding, AboutViewModel>(), AboutNavigator {
    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var mAboutViewModel: AboutViewModel

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_about

    override val viewModel: AboutViewModel
        get() {
            mAboutViewModel = ViewModelProvider(this, factory).get(AboutViewModel::class.java)
            return mAboutViewModel
        }

    override fun goBack() {
        baseActivity?.onFragmentDetached(TAG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAboutViewModel.navigator = this
    }

    companion object {

        val TAG: String = AboutFragment::class.java.simpleName

        fun newInstance(): AboutFragment {
            val args = Bundle()
            val fragment = AboutFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
}