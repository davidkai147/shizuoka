package jp.shizuoka.ui.about

interface AboutNavigator {

    fun goBack()
}