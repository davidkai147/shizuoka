package jp.shizuoka.ui.about

import jp.shizuoka.data.DataManager
import jp.shizuoka.ui.base.BaseViewModel
import jp.shizuoka.utils.rx.SchedulerProvider

class AboutViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<AboutNavigator>(dataManager, schedulerProvider) {

    fun onNavBackClick() {
        navigator?.goBack()
    }
}