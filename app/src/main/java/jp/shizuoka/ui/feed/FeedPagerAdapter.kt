package jp.shizuoka.ui.feed

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import jp.shizuoka.ui.feed.opensource.OpenSourceFragment
import jp.shizuoka.ui.feed.blogs.BlogFragment
import androidx.fragment.app.FragmentStatePagerAdapter

class FeedPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var mTabCount: Int = 0

    init {
        this.mTabCount = 0
    }

    override fun getCount(): Int {
        return mTabCount
    }

    fun setCount(count: Int) {
        mTabCount = count
    }

    override fun getItem(position: Int): Fragment {
        lateinit var returnFragment: Fragment
        when (position) {
            0 -> {
                returnFragment = BlogFragment()
            }
            1 -> {
                returnFragment = OpenSourceFragment.newInstance()
            }
        }
        return returnFragment
    }
}