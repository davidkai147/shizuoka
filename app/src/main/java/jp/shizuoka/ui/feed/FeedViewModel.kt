package jp.shizuoka.ui.feed

import jp.shizuoka.data.DataManager
import jp.shizuoka.ui.base.BaseViewModel
import jp.shizuoka.utils.rx.SchedulerProvider

class FeedViewModel : BaseViewModel<Nothing> {
    constructor(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ) : super(dataManager, schedulerProvider)

}