package jp.shizuoka.ui.feed.blogs

class BlogEmptyItemViewModel(private val mListener: BlogEmptyItemViewModelListener) {

    fun onRetryClick() {
        mListener.onRetryClick()
    }

    interface BlogEmptyItemViewModelListener {

        fun onRetryClick()
    }
}