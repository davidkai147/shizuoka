package jp.shizuoka.ui.feed.blogs

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import jp.shizuoka.BR
import jp.shizuoka.R
import jp.shizuoka.ViewModelProviderFactory
import jp.shizuoka.data.model.api.BlogResponse
import jp.shizuoka.databinding.FragmentBlogBinding
import jp.shizuoka.ui.base.BaseFragment
import javax.inject.Inject

class BlogFragment : BaseFragment<FragmentBlogBinding, BlogViewModel>(), BlogNavigator,
    BlogAdapter.BlogAdapterListener {

    @Inject
    lateinit var mBlogAdapter: BlogAdapter
    private var mFragmentBlogBinding: FragmentBlogBinding? = null
    @Inject
    lateinit var mLayoutManager: LinearLayoutManager
    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var mBlogViewModel: BlogViewModel

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_blog

    override val viewModel: BlogViewModel
        get() {
            mBlogViewModel = ViewModelProvider(this, factory).get(BlogViewModel::class.java)
            return mBlogViewModel
        }

    override fun handleError(throwable: Throwable) {
        // handle error
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBlogViewModel.navigator = this
        mBlogAdapter.setListener(this)
    }

    override fun onRetryClick() {
        mBlogViewModel.fetchBlogs()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFragmentBlogBinding = viewDataBinding
        setUp()
    }

    override fun updateBlog(blogList: List<BlogResponse.Blog>?) {
        if (blogList != null) {
            mBlogAdapter.addItems(blogList)
        }
    }

    private fun setUp() {
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        mFragmentBlogBinding?.blogRecyclerView?.layoutManager = mLayoutManager
        mFragmentBlogBinding?.blogRecyclerView?.itemAnimator = DefaultItemAnimator()
        mFragmentBlogBinding?.blogRecyclerView?.adapter = mBlogAdapter
    }

    companion object {

        fun newInstance(): BlogFragment {
            val args = Bundle()
            val fragment = BlogFragment()
            fragment.arguments = args
            return fragment
        }
    }
}