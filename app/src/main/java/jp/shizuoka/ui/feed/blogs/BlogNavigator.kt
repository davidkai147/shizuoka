package jp.shizuoka.ui.feed.blogs

import jp.shizuoka.data.model.api.BlogResponse


interface BlogNavigator {

    fun handleError(throwable: Throwable)

    fun updateBlog(blogList: List<BlogResponse.Blog>?)
}