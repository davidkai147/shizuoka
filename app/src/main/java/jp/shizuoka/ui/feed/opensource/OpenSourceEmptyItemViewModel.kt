package jp.shizuoka.ui.feed.opensource

class OpenSourceEmptyItemViewModel(private val mListener: OpenSourceEmptyItemViewModelListener) {

    fun onRetryClick() {
        mListener.onRetryClick()
    }

    interface OpenSourceEmptyItemViewModelListener {

        fun onRetryClick()
    }
}