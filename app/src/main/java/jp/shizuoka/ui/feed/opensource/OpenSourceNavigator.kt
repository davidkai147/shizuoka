package jp.shizuoka.ui.feed.opensource

interface OpenSourceNavigator {

    fun handleError(throwable: Throwable)
}