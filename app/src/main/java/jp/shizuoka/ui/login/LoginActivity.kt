package jp.shizuoka.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import jp.shizuoka.BR
import jp.shizuoka.ui.main.MainActivity
import jp.shizuoka.R
import jp.shizuoka.ViewModelProviderFactory
import jp.shizuoka.databinding.ActivityLoginBinding
import jp.shizuoka.ui.base.BaseActivity
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginNavigator {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var mLoginViewModel: LoginViewModel
    private var mActivityLoginBinding: ActivityLoginBinding? = null

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_login

    override val viewModel: LoginViewModel
        get() {
            mLoginViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
            return mLoginViewModel
        }

    override fun handleError(throwable: Throwable) {
        // handle error
    }

    override fun login() {
        val email = mActivityLoginBinding!!.etEmail.getText().toString()
        val password = mActivityLoginBinding!!.etPassword.getText().toString()
        if (mLoginViewModel.isEmailAndPasswordValid(email, password)) {
            hideKeyboard()
            mLoginViewModel.login(email, password)
        } else {
            Toast.makeText(this, getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show()
        }
    }

    override fun openMainActivity() {
        val intent = MainActivity.newIntent(this@LoginActivity)
        startActivity(intent)
        finish()
    }

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityLoginBinding = viewDataBinding
        mLoginViewModel.navigator = this
    }

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}