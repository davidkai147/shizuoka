package jp.shizuoka.ui.main

interface MainNavigator {

    fun handleError(throwable: Throwable)

    fun openLoginActivity()
}