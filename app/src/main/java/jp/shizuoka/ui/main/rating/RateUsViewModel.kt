package jp.shizuoka.ui.main.rating

import jp.shizuoka.data.DataManager
import jp.shizuoka.ui.base.BaseViewModel
import jp.shizuoka.utils.rx.SchedulerProvider

class RateUsViewModel : BaseViewModel<RateUsCallback> {
    constructor(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ) : super(dataManager, schedulerProvider)

    fun onLaterClick() {
        navigator?.dismissDialog()
    }

    fun onSubmitClick() {
        navigator?.dismissDialog()
    }
}