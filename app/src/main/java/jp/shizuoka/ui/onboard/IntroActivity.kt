package jp.shizuoka.ui.onboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import jp.shizuoka.R
import jp.shizuoka.ui.main.MainActivity
import jp.shizuoka.ui.onboard.lib.OnboarderActivity
import jp.shizuoka.ui.onboard.lib.OnboarderPage


class IntroActivity : OnboarderActivity() {

    var onboarderPages: ArrayList<OnboarderPage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onboarderPages = ArrayList()

        // Create your first page
        val onboarderPage1 = OnboarderPage("", "", R.mipmap.die_for_music)
        val onboarderPage2 = OnboarderPage("", "", R.mipmap.die_for_music)
        val onboarderPage3 = OnboarderPage("", "", R.mipmap.die_for_music)

        // You can define title and description colors (by default white)
        onboarderPage1.setTitleColor(R.color.black)
        onboarderPage1.setDescriptionColor(R.color.white)

        // Don't forget to set background color for your page
        onboarderPage1.setBackgroundColor(R.color.colorPrimary)
        onboarderPage2.setBackgroundColor(R.color.google)
        onboarderPage3.setBackgroundColor(R.color.fb)

        // Add your pages to the list
        onboarderPages!!.add(onboarderPage1)
        onboarderPages!!.add(onboarderPage2)
        onboarderPages!!.add(onboarderPage3)

        setDividerHeight(0)
        setSkipButtonHidden()
        shouldDisplayCloseButtonOnLastPage(true)

        // And pass your pages to 'setOnboardPagesReady' method
        setOnboardPagesReady(onboarderPages)
    }

    override fun onFinishButtonPressed() {
        val intent = MainActivity.newIntent(this@IntroActivity)
        startActivity(intent)
        finish()
    }

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, IntroActivity::class.java)
        }
    }
}
