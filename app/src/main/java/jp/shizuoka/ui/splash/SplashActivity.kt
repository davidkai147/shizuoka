package jp.shizuoka.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProvider
import jp.shizuoka.BR
import jp.shizuoka.ui.main.MainActivity
import jp.shizuoka.R
import jp.shizuoka.ViewModelProviderFactory
import jp.shizuoka.databinding.ActivitySplashBinding
import jp.shizuoka.ui.base.BaseActivity
import jp.shizuoka.ui.onboard.IntroActivity
import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(), SplashNavigator {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var mSplashViewModel: SplashViewModel

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_splash

    override val viewModel: SplashViewModel
        get() {
            mSplashViewModel = ViewModelProvider(this, factory).get(SplashViewModel::class.java)
            return mSplashViewModel
        }

    override fun openIntroActivity() {
        val r = Runnable {
            val intent = IntroActivity.newIntent(this@SplashActivity)
            startActivity(intent)
            finish()
        }
        Handler().postDelayed(r, 2000)
    }

    override fun openMainActivity() {
        val r = Runnable {
            val intent = MainActivity.newIntent(this@SplashActivity)
            startActivity(intent)
            finish()
        }
        Handler().postDelayed(r, 2000)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSplashViewModel.navigator = this
        mSplashViewModel.startSeeding()
    }
}