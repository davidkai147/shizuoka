package jp.shizuoka.ui.splash

interface SplashNavigator {

    fun openIntroActivity()

    fun openMainActivity()
}