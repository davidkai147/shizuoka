package jp.shizuoka.ui.splash

import jp.shizuoka.data.DataManager
import jp.shizuoka.ui.base.BaseViewModel
import jp.shizuoka.utils.AppLogger
import jp.shizuoka.utils.rx.SchedulerProvider

class SplashViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<SplashNavigator>(dataManager, schedulerProvider) {

    fun startSeeding() {
        compositeDisposable.add(
            dataManager
                .seedDatabaseQuestions()
                .flatMap { aBoolean -> dataManager.seedDatabaseOptions() }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    { aBoolean ->
                        decideNextActivity()
                        AppLogger.i("SplashViewModel $aBoolean")
                        getAllQuestion()
                    },
                    { throwable ->
                        decideNextActivity()
                        AppLogger.d(throwable, "SplashViewModel")
                    })
        )
    }

    private fun decideNextActivity() {
        //if (dataManager.currentUserLoggedInMode == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type) {
        if (!dataManager.isDoneFirstLoad) {
            dataManager.updateFirstLoad(true)
            navigator?.openIntroActivity()
        } else {
            navigator?.openMainActivity()
        }
    }

    fun getAllQuestion() {
        compositeDisposable.add(
            dataManager.allQuestions
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ listQuestion ->
                    AppLogger.i("SplashViewModel ${listQuestion[0].questionText}")
                },
                    { throwable ->
                        AppLogger.d(throwable, "SplashViewModel")
                    })
        )
    }
}